package incsearch

import org.scalatest.prop.GeneratorDrivenPropertyChecks

object Benchmark extends App with InputGenerators with GeneratorDrivenPropertyChecks {
  /**
    * Run the given code, returning the time in nanos
    */
  private def timing[A](f: => A): Long = {
    val t1 = System.nanoTime()
    f
    val t2 = System.nanoTime()
    t2 - t1
  }

  var timeSimple = 0L
  var timeBisect = 0L

  forAll(testCaseGen) { case (corpus, input) =>
    val simpleSearcher = SimpleSearcher(corpus)
    timeSimple += timing(simpleSearcher.search(input))
    val bisectSearcher = BisectSearcher(corpus)
    timeBisect += timing(bisectSearcher.search(input))
  }

  println(s"Time with SimpleSearcher: ${timeSimple / 1E6} ms")
  println(s"Time with BisectSearcher: ${timeBisect / 1E6} ms")
}
