package incsearch

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class BissectSearcherTest extends FreeSpec with GeneratorDrivenPropertyChecks with InputGenerators with Matchers {
  "BissectSearcher behaves like SimpleSearcher" in {
    forAll(testCaseGen) { case (corpus, input) =>
      BisectSearcher(corpus).search(input) should ===(SimpleSearcher(corpus).search(input))
    }
  }
}
