package incsearch

import org.scalacheck.Gen

trait InputGenerators {
  // Generator for sentences of alphanum chars
  // If we try to run the tests with arbitrary characters, we'll end up
  // with no results most of the time because it's unlikely to find a common prefix with the full
  // range of characters
  val stringGen: Gen[String] = Gen.nonEmptyListOf(Gen.alphaNumStr).map(_.mkString(" "))

  val prefixGen: Gen[String] = for {
    char <- Gen.alphaNumChar
    size <- Gen.oneOf(1, 2, 3, 4)
    str <- stringGen
  } yield char +: str.take(size)

  val testCaseGen: Gen[(List[String], String)] = Gen.zip(Gen.listOf(stringGen), prefixGen)
}

object InputGenerators extends InputGenerators
