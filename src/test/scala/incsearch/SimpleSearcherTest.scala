package incsearch

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}

class SimpleSearcherTest extends FreeSpec with Matchers with GeneratorDrivenPropertyChecks with InputGenerators {
  "SimpleSearcher returns the first words with a given prefix" in {
    SimpleSearcher(Seq("word", "abc", "abd", "abe", "abf", "abg")).search("ab") should ===(
      Vector("abc", "abd", "abe", "abf")
    )

    forAll(testCaseGen) { case (corpus: Seq[String], input: String) =>
      val results = SimpleSearcher(corpus).search(input)
      // check size
      results.size shouldBe < (Searcher.numResult + 1)

      // check results are appropriate
      results.forall(_.startsWith(input)) shouldBe true

      // check inclusion
      val lowercasedCorpus = corpus.map(_.toLowerCase)
      results.forall(lowercasedCorpus.contains) shouldBe true

      // check ordering
      results should ===(results.sorted)
    }
  }

  "SimpleSeacher works with the example input" in {
    val corpus = Seq(
      "Bowl",
      "Kayak",
      "Owl",
      "Pandora",
      "Paypal",
      "Pg&e",
      "Pinterest",
      "Prank",
      "Press democrat",
      "Print",
      "Proactive",
      "Processor",
      "Procurable",
      "Progenex",
      "Progeria",
      "Progesterone",
      "Programming",
      "Progressive",
      "Project free tv Priceline",
      "Project runway",
      "Proud",
      "Reprobe",
      "River",
      "Stamps"
    )

    val searcher = SimpleSearcher(corpus)

    searcher.search("p") should ===(List(
      "pandora",
      "paypal",
      "pg&e",
      "pinterest"
    ))

    searcher.search("pr") should ===(List(
      "prank",
      "press democrat",
      "print",
      "proactive"
    ))

    searcher.search("pro") should ===(List(
      "proactive",
      "processor",
      "procurable",
      "progenex"
    ))

    searcher.search("prog") should ===(List(
      "progenex",
      "progeria",
      "progesterone",
      "programming"
    ))
  }
}
