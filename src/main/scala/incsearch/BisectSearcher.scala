package incsearch

import scala.annotation.tailrec

/**
  * This Searcher works by
  *   - sorting the corpus
  *   - finding the smaller word that starts by the input, using binary search
  *   - simply take the next words, until we have the desired numbered of result, or until they stop starting with the given prefix
  */

case class BisectSearcher(corpus: Seq[String]) extends Searcher {
  private val sortedCorpus = sanitizedCorpus.sorted.toVector

  @tailrec
  private def findIndex(prefix: String, start: Int, end: Int, bestCandidate: Option[Int]): Option[Int] =
    if (start >= end)
      bestCandidate
    else {
      val middleIndex = start + (end - start) / 2
      val middle = sortedCorpus(middleIndex)
      if (middle.startsWith(prefix))
        findIndex(prefix, start, middleIndex - 1, Some(middleIndex))
      else if (middle > prefix)
        findIndex(prefix, start, middleIndex - 1, bestCandidate)
      else
        findIndex(prefix, middleIndex + 1, end, bestCandidate)
    }

  override def search(input: String): List[String] = {
    val index = findIndex(input, 0, sortedCorpus.size, None)

    index.fold(List.empty[String]) { i =>
      sortedCorpus.slice(i, i + Searcher.numResult).takeWhile(_.startsWith(input)).toList
    }
  }
}
