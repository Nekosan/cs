package incsearch

import Searcher.numResult

case class SimpleSearcher(corpus: Seq[String]) extends Searcher {
  override def search(input: String): List[String] =
    sanitizedCorpus.filter(_.startsWith(input)).sorted.take(numResult).toList
}
