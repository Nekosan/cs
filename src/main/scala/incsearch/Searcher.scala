package incsearch

object Searcher {
  /**
    * Maximum number of results returned by the incremental search
    */
  val numResult = 4
}

trait Searcher {
  /**
    *
    * @param input Common prefix to all the words returned by the search
    * @return `Searcher.numResult` first words from the specified `corpus` with the given prefix, by ascending alphabetical order
    */
  def search(input: String): List[String]

  def corpus: Seq[String]

  /**
    * The sanitized corpus is made of lower-cased sentences, with no blank string
    */
  protected lazy val sanitizedCorpus: Seq[String] =
    corpus.map(_.toLowerCase).filter(_.nonEmpty)
}
