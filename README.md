The trait `Searcher` allows, given a corpus of sentences and an input, to find the words from the corpus that begins with the given prefix. The results are limited by `Searcher.numResult`

I provided 2 implementations for `Searcher`:

- `SimpleSearcher` is a naive one, that simply filters the corpus. This is used mostly as a reference implementation to test faster, more complex implementations.
- `BisectSearcher` sorts the corpus, finds the smallest appropriate candidate, then get the next items in the sorted corpus.

I included a simple `Benchmark` to have a first idea of the relative performances. An example of the run yields:

```
Time with SimpleSearcher: 20.512216 ms
Time with BisectSearcher: 1.894347 ms
```
